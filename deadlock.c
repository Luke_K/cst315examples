#include <stdio.h>
#include <pthread.h> 
#include <semaphore.h> /* For semaphore */
#include <sys/mman.h>  /* For mmap */
#include <unistd.h>    /* For _exit */
#include <sys/types.h> /* For waitpid */
#include <sys/wait.h>  /* For waitpid */
#include <signal.h>    /* For sigaction */

/* Constants */

/* Global variables */
sem_t* resourceA;
sem_t* resourceB;

pid_t pid_parent;
pid_t pid_child1;
pid_t pid_child2;


/* Signal handler functions */
void child1_handler(int signum) {
  /* Clean up after ourself, we're going away */
  printf("Child 1 received signal %d, cleaning up...\n", signum);
  
  /* We're assuming that we're holding resource A */
  sem_post(resourceA);

  /* When done, exit */
  printf("Child 1 exiting...\n");
  _exit(0);
}

/* Process functions (children & parent, etc.) */
void child_1() {
  /* Do stuff */
  printf("Hello from child 1\n");

  /* Setup signal handling */
  struct sigaction act;
  act.sa_handler = child1_handler;
  sigaction(SIGUSR1, &act, NULL);

  /* Grab resource A */
  printf("Child 1 going after resource A...\n");
  sem_wait(resourceA);
  printf("Child 1 received resource A!\n");

  usleep(500);

  /* Grab resource B */
  printf("Child 1 going after resource B...\n");
  sem_wait(resourceB);
  printf("Child 1 received resource B!\n");

  /* Announce success */
  printf("Child 1 got both of the needed resources!\n");

  /* Return resources */
  sem_post(resourceA);
  sem_post(resourceB);

  printf("Child 1 is done, exiting...\n");

  /* When done, exit */
  _exit(0);
}

void child_2() {
  /* Do stuff */
  printf("Hello from child 2\n");

  /* Grab resource B */
  printf("Child 2 going after resource B...\n");
  sem_wait(resourceB);
  printf("Child 2 received resource B!\n");

  usleep(500);

  /* Grab resource A */
  printf("Child 2 going after resource A...\n");
  sem_wait(resourceA);
  printf("Child 2 received resource A!\n");

  /* Announce success */
  printf("Child 2 got both of the needed resources!\n");

  /* Return resources */
  sem_post(resourceA);
  sem_post(resourceB);

  printf("Child 2 is done, exiting...\n");

  /* When done, exit */
  _exit(0);
}

void parent() {
  /* Do stuff */
  printf("Hello from parent\n");

  int s = 3;
  printf("Parent sleeping for %d seconds...\n", s);
  int i;
  for (i = s; i > 0; --i) {
    printf("%d...\n", i);
    sleep(1);
  }
  printf("Parent done sleeping.\n");

  printf("Sending SIGUSR1 to child 1...\n");
  kill(pid_child1, SIGUSR1);
  printf("Sent signal.\n");

  /* When done, just return */
}

/* Helper functions */

/* Main */
int main() {
  /* Initialize shared memory spaces */
  resourceA = (sem_t*)mmap(0,
                           sizeof(sem_t),
                           PROT_READ|PROT_WRITE,
                           MAP_SHARED|MAP_ANONYMOUS,
                           -1,
                           0);
  resourceB = (sem_t*)mmap(0,
                           sizeof(sem_t),
                           PROT_READ|PROT_WRITE,
                           MAP_SHARED|MAP_ANONYMOUS,
                           -1,
                           0);

  /* Initialize resources (semaphores) */
  sem_init(resourceA, 1, 1);
  sem_init(resourceB, 1, 1);

  /* Create children */
  pid_t pid;

  pid = fork();

  if (pid == 0) { /* Child 1 */
    child_1();
  }
  else {  /* Parent */
    pid_child1 = pid;
    
    pid = fork();

    if (pid == 0) { /* Child 2 */
      child_2();
    }
    else { /* Parent */
      pid_child2 = pid;

      parent();
    }
  }

  /* Wait on children */
  printf("Parent waiting on children...\n");
  waitpid(pid_child1, NULL, 0);
  waitpid(pid_child2, NULL, 0);

  printf("Parent exiting\n");

  return 0;
}
