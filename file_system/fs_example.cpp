/*******************************************************************************
* File: fs_example.cpp
* Author: Luke Kanuchok
* Date: 2018/04/16
*
* Requires C++11 flag: "g++ -std=c++11 fs_example.cpp"
*
* Demonstrates some basic filesystem properties using the previous io_example.
*
* Note: This example of a file system is incomplete, but should provide some
* direction that will assist in developing a more fully fledged file system.
*******************************************************************************/

#include <iostream>
#include <fstream>
#include <iomanip> //For std::setfill
#include <sstream> //For reading integers
#include <utility> //For std::pair
#include <string.h>  //For strncpy
#include <vector>

#include <sys/stat.h>  //For stat
#include <fcntl.h>  //For open
#include <sys/types.h> //For open flags
#include <unistd.h>  //For write

/* General functions */

std::vector<std::string> tokenize(std::string input_str, const char delimiter) {
  std::stringstream ss(input_str);
  std::vector<std::string> tokens;
  std::string tmp;
  while (std::getline(ss, tmp, delimiter)) {
    tokens.push_back(tmp);
  }
  return tokens;
}

/* Class declarations */

class Disk {
  public:
  Disk();
  ~Disk();

  //Connect (open file system disk file), and prepare for operation
  void start();

  //Finish up all writes, and disconnect from the disk
  void finalize();
  
  //Write all zeroes to the disk
  void clear();

  //Make a backup of the disk
  void create_backup(std::string backup_filename);

  //Re-load the backup
  void restore_backup(std::string backup_filename);

  //Check for drive file existence
  static bool drive_exists(const std::string drive_filename);

  //Destroy the drive file (remove the disk file)
  static void destroy(const std::string drive_filename);

  //Create a drive (tests for existence, and silently returns if disk exists)
  static void create(const std::string drive_filename);

  //Set the disk location
  void seek_to(const int location);

  //Write an integer to a given location/address on "disk"
  void writeInt(const int data, const long address);

  //Read an integer from a given address on "disk"
  void readInt(int& data, const int address);

  //Write a single character out to a file, at a given address
  void writeChar(const char data, const int address);

  //Read a character/byte from a given address on "disk"
  void readChar(char& data, const int address);

  //Write an array of characters out to a file, at a given address
  void writeChars(const char* data, const int count, const int address);

  //Read a character/byte from a given address on "disk"
  void readChars(char* data, const int count, const int address);

  const static std::string filesystem_filename;
  const static std::string backup_filename;


  const static size_t blocksize_in_bytes = 4096;
  const static size_t disksize_in_blocks = 32;
  const static size_t disksize_in_bytes = disksize_in_blocks *
                                          blocksize_in_bytes;
  private:
  //The disk for supporting our file system
  const std::ios_base::openmode accessflags;

  //Our access to the disk
  std::fstream fs;
};
const std::string Disk::filesystem_filename = "filesys.dat";
const std::string Disk::backup_filename = "filesys.bkup";

//Abstract base class for structures to be stored on disk
class SystemDataStructure {
  public:

  //Write this object to disk
  virtual void serialize(Disk& disk) const = 0;
  //Read this object from disk
  virtual void deserialize(Disk& disk) = 0;
  protected:
  private:
};

//Demonstrate the use of SystemDataStructure
const int PUBLIC_CHAR_ARR_SIZE = 6;
const int PRIVATE_CHAR_ARR_SIZE = 5;
class SystemDemo : public SystemDataStructure {
  public:
  SystemDemo();

  //Write this object to disk
  void serialize(Disk& disk) const;
  //Read this object from disk
  void deserialize(Disk& disk);

  friend std::ostream& operator<<(std::ostream& os, SystemDemo& sd);

  int public_int_val;
  char public_char_val;
  char public_char_arr[PUBLIC_CHAR_ARR_SIZE];
  protected:
  private:
  int private_int_val;
  char private_char_val;
  char private_char_arr[PRIVATE_CHAR_ARR_SIZE];
};


class BasicFS : public SystemDataStructure {
  public:
  BasicFS(Disk& disk_);

  //Write this object to disk
  void serialize(Disk& disk) const;
  //Read this object from disk
  void deserialize(Disk& disk);

  friend std::ostream& operator<<(std::ostream& os, BasicFS& bfs);

  //Class constants

  //Each directory has no more than this number of file and directories
  //(Including the . and .. relative directory paths
  const static int MAX_NUM_FILES_PER_DIR = 10;
  const static int MAX_FILENAME_LEN = 12;
  const static int INODES_PER_BLOCK = 16;

  //Internal classes
  class BasicBlock;   //A block of data (also used as a absract data type)
  class Inode;        //Index node for a file or directory (size: 1/16 block)
  class Dentry;       //Data storage for a directory (size: 1 data block)
  class Superblock;   //High level information about the file system as a whole
  class File;         //Information about an open file
  class IsUsedBitmap; //Bitmap for tracking inode and datablock usage

  //Filesystem manipulation routines (low-level)

  //Seek to the position of a given inode
  void seekToInode(int pos);

  //Write an inode entry into an inode slot
  void writeInode(const Inode& inode, int pos);

  //Read an inode entry from an inode slot
  void readInode(Inode& inode, int pos);

  //Seek to the position of a given data block
  void seekToDataBlock(int pos);

  //Write a given dentry to a data block
  void writeDentry(int pos);

  //Read a dentry from a given data block
  void readDentry(int pos);



  //Basic getter of inode object/entry from inode number
  Inode getInodeByNumber(int inode_num);

  //Returns inode number of the file/directory string, or -1 if not found
  int getInodeFromPath(std::string path);

  //Returns inode number of the directory containing path given by string,
  //or -1 if not found
  int getParentFromPath(std::string path);

  //Returns a Dentry object that is located at provided inode
  Dentry getDentryByInode(int inode);

  //Returns a Basic Block object that is located at provided inode
  BasicBlock getBlockByInode(int inode);

  //Returns an absolute address from provided inode number (or -1 if failure)
  int getAddrFromInode(int inode);

  static constexpr char pathSep() {return '/';}

  protected:
  private:
  Superblock* supblk;
  Disk& disk;
};

class BasicFS::Inode : public SystemDataStructure {
  public:
  Inode();
  //If we already know where the data block is...
  Inode(int dbn);

  //Get the data block number
  int getDBN();

  //Get the offset from the beginning of the data blocks
  size_t getAddress();

  //Write this object to disk
  void serialize(Disk& disk) const;
  //Read this object from disk
  void deserialize(Disk& disk);

  friend std::ostream& operator<<(std::ostream& os, Inode& ind);

  protected:
  private:
  int data_block_num; //Location of the corresponding data in data region
};

class BasicFS::Dentry : public SystemDataStructure {
  public:
  Dentry();
  //If we know the parents inode number, we can set '..' more easily
  Dentry(int parent_inode_num, int my_inode_num);

  //Write this object to disk
  void serialize(Disk& disk) const;
  //Read this object from disk
  void deserialize(Disk& disk);
  void addFile(std::string fname, int inode_num);
  void updateInode(std::string fname, int new_inode_num);
  void removeFile(std::string fname);

  friend std::ostream& operator<<(std::ostream& os, Dentry& de);

  protected:
  private:
  //Each pair contains a filename and an inode number,
  //and the array holds the . (current directory) and .. (parent directory)
  //inode numbers
  //Note: file system root will have itself as its parent
  std::pair<char[MAX_FILENAME_LEN], int> files[MAX_NUM_FILES_PER_DIR];
  //A count of the number of entries used, including the . and .. (should
  //never go below 2!)
  int num_entries_used;
};

class BasicFS::IsUsedBitmap : public SystemDataStructure {
  public:
  //Construct a bitmap with the number of locations
  IsUsedBitmap(const int num_values);
  ~IsUsedBitmap();

  //Write this object to disk
  void serialize(Disk& disk) const;
  //Read this object from disk
  void deserialize(Disk& disk);

  //Set all values to 'cleared'
  void clear();

  //Set a bit as given position
  void set(unsigned int pos);
  //Clear a bit at given position
  void clr(unsigned int pos);
  //Test (return) the bit at given position
  bool tst(unsigned int pos);

  friend std::ostream& operator<<(std::ostream& os, IsUsedBitmap& iub);

  //Corresponds to the value is set/used
  static constexpr char bitval_set() { return '1'; }
  //Corresponds to the value is available/cleared
  static constexpr char bitval_clr() { return '0'; }
  protected:
  private:
  unsigned int size; //number of values in this bitmap
  char* bitmap;
};

class BasicFS::Superblock : public SystemDataStructure {
  public:
  Superblock();
  Superblock(int root_inode_,
             int count_superblock,
             int count_inode_bitmap,
             int count_data_bitmap,
             int count_inodes,
             int count_data,
             int start_superblock,
             int start_inode_bitmap,
             int start_data_bitmap,
             int start_inodes,
             int start_data);
  //Write this object to disk
  void serialize(Disk& disk) const;
  //Read this object from disk
  void deserialize(Disk& disk);

  // General
  const unsigned int root_inode = 1;

  //Region sizes
  const unsigned int BLOCK_COUNT_SUPERBLOCK = 1;
  const unsigned int BLOCK_COUNT_INODE_BITMAP = 1;
  const unsigned int BLOCK_COUNT_DATA_BITMAP = 1;
  const unsigned int BLOCK_COUNT_INODES = 3;
  const unsigned int BLOCK_COUNT_DATA = 26;
  
  //Block indexes
  const unsigned int BLOCK_START_SUPERBLOCK = 0;
  const unsigned int BLOCK_START_INODE_BITMAP = BLOCK_START_SUPERBLOCK +
                                                BLOCK_COUNT_SUPERBLOCK;
  const unsigned int BLOCK_START_DATA_BITMAP = BLOCK_START_INODE_BITMAP +
                                               BLOCK_COUNT_INODE_BITMAP;
  const unsigned int BLOCK_START_INODES = BLOCK_START_DATA_BITMAP +
                                          BLOCK_COUNT_DATA_BITMAP;
  const unsigned int BLOCK_START_DATA = BLOCK_START_INODES +
                                        BLOCK_COUNT_INODES;
  protected:
  private:
};

// - Disk methods -

Disk::Disk()
  : accessflags(std::ios::in|std::ios::out) {
  start();
}

Disk::~Disk() {
  finalize();
}


void Disk::start() {
  fs.open(filesystem_filename, accessflags);
  std::cout << "Disk opened, system started" << std::endl;
}

void Disk::finalize() {
  fs.close();
  std::cout << "Disk closed, system halted" << std::endl;
}

void Disk::clear() {
  //Save the current location
  long saved_pos = fs.tellp();

  //Go to the beginning of the disk
  fs.seekp(0);  //Seek for (out)Put operation 
  for (int i = 0; i < disksize_in_bytes; ++i)
    fs.put(0);

  //Go back to where we were
  fs.seekp(saved_pos);
}

//Make a backup of the disk
void Disk::create_backup(std::string backup_filename) {
  std::ofstream bkup(backup_filename);
  //Save the current location
  long saved_pos = fs.tellp();

  //Go to the beginning of the disk
  fs.seekp(0);  //Seek for (out)Put operation 
  bkup << fs.rdbuf();

  bkup.close();

  //Go back to where we were
  fs.seekp(saved_pos);
}

//Set the disk location
void Disk::seek_to(const int location) {
  fs.seekg(location);
}

//Re-load the backup
void Disk::restore_backup(std::string backup_filename) {
  std::ifstream bkup(backup_filename);
  //Save the current location
  long saved_pos = fs.tellp();

  //Go to the beginning of the disk
  fs.seekp(0);  //Seek for (out)Put operation 
  fs << bkup.rdbuf();

  bkup.close();

  //Go back to where we were
  fs.seekp(saved_pos);
}

//Check for drive file existence
bool Disk::drive_exists(const std::string drive_filename) {
  struct stat buffer;
  return (stat (drive_filename.c_str(), &buffer) == 0);
}

//Destroy the drive file (remove the disk file)
void Disk::destroy(const std::string drive_filename =
                          Disk::filesystem_filename) {
  if (Disk::drive_exists(drive_filename))
    remove(drive_filename.c_str());
}

//Create a drive (tests for existence, and silently returns if disk exists)
void Disk::create(const std::string drive_filename =
                         Disk::filesystem_filename) {
  if (!Disk::drive_exists(drive_filename)) {
    //Low level disk creation
    char blk_buf[Disk::blocksize_in_bytes];
    std::fill_n(blk_buf, Disk::blocksize_in_bytes, 0);
    int fd = open(drive_filename.c_str(), O_CREAT|O_WRONLY, S_IRUSR|S_IWUSR);
    for (int i = 0; i < Disk::disksize_in_blocks; ++i)
      write(fd, blk_buf, Disk::blocksize_in_bytes);
    close(fd);
  }
}

//Write an integer to a given location/address on "disk"
//If the address is less than zero (default), then write
//to the current position.
//Zero padded to 10 characters, right justified, as we may
//Want to store large numbers in a fixed field size
void Disk::writeInt(const int data, const long address = -1) {
  //fmt flags code adapted from:
  //https://stackoverflow.com/a/2273352
  std::ios::fmtflags saved_format(fs.flags());
  if (address >= 0) //Seek to a new position
    fs.seekp(address);

  fs.fill('0');
  fs.width(10);
  fs << data;

  fs.flags(saved_format);
}

//Read an integer from a given address on "disk"
//If the address is less than zero (default), then read
//from the current position
void Disk::readInt(int& data, const int address = -1) {
  if (address >= 0) //Seek to a new position
    fs.seekg(address);

  char buf[10];
  fs.read(buf, 10);
  std::stringstream ss(buf);
  ss >> data;
}

//Write a single character out to a file, at a given address
void Disk::writeChar(const char data, const int address = -1) {
  if (address >= 0) //Seek to a new position
    fs.seekg(address);
  
  fs << data;
}

//Read a character/byte from a given address on "disk"
void Disk::readChar(char& data, const int address = -1) {
  if (address >= 0) //Seek to a new position
    fs.seekg(address);
  
  fs >> data;
}

//Write an array of characters out to a file, at a given address
void Disk::writeChars(const char* data,
                      const int count,
                      const int address = -1) {
  if (address >= 0) //Seek to a new position
    fs.seekp(address);

  fs.write(data, count);
}

//Read a character/byte from a given address on "disk"
void Disk::readChars(char* data, const int count, const int address = -1) {
  if (address >= 0) //Seek to a new position
    fs.seekg(address);
  fs.read(data, count);
}

// - SystemDemo methods -

SystemDemo::SystemDemo()
  : public_int_val(42),
    public_char_val('x'),
    public_char_arr{'a','b','c','d','e'},
    private_int_val(200),
    private_char_val('f'),
    private_char_arr{'w','x','y','z'}
    {}

//It is assumed that the disk file stream has been put to the corect position
void SystemDemo::serialize(Disk& disk) const {
  disk.writeInt(public_int_val);
  disk.writeChar(public_char_val);
  disk.writeChars(public_char_arr, PUBLIC_CHAR_ARR_SIZE);
  disk.writeInt(private_int_val);
  disk.writeChar(private_char_val);
  disk.writeChars(private_char_arr, PRIVATE_CHAR_ARR_SIZE);
}

//It is assumed that the disk file stream has been put to the corect position
void SystemDemo::deserialize(Disk& disk) {
  disk.readInt(public_int_val);
  disk.readChar(public_char_val);
  disk.readChars(public_char_arr, PUBLIC_CHAR_ARR_SIZE);
  disk.readInt(private_int_val);
  disk.readChar(private_char_val);
  disk.readChars(private_char_arr, PRIVATE_CHAR_ARR_SIZE);
}

std::ostream& operator<<(std::ostream& os, SystemDemo& sd) {
  os << "public int val: " << sd.public_int_val << std::endl;
  os << "public char val: " << sd.public_char_val << std::endl;
  os << "public char arr: " << sd.public_char_arr << std::endl;
  os << "private int val: " << sd.private_int_val << std::endl;
  os << "private char val: " << sd.private_char_val << std::endl;
  os << "private char arr: " << sd.private_char_arr << std::endl;
  return os;
}

// - BasicFS methods -

BasicFS::BasicFS(Disk& disk_) :
  supblk(new Superblock()),
  disk(disk_) {}

//Write this object to disk
void BasicFS::serialize(Disk& disk) const {
  throw std::runtime_error("Not implemented yet!");
}

//Read this object from disk
void BasicFS::deserialize(Disk& disk) {
  throw std::runtime_error("Not implemented yet!");
}

//Returns a Dentry object that is located at provided inode
BasicFS::Dentry BasicFS::getDentryByInode(int inode) {
  Dentry dtry;
  //Read inode for disk location

  throw std::runtime_error("Not implemented yet!");
}

//Seek to the position of a given inode
void BasicFS::seekToInode(int pos) {
  //Calculate inode offset
  int start_inodes = supblk->BLOCK_START_INODES * Disk::blocksize_in_bytes;
  int local_offset = pos * (Disk::blocksize_in_bytes / INODES_PER_BLOCK);
  disk.seek_to(start_inodes + local_offset);
}

//Write an inode entry into an inode slot
void BasicFS::writeInode(const Inode& inode, int pos) {
  seekToInode(pos);
  inode.serialize(disk);
}

//Read an inode entry from an inode slot
void BasicFS::readInode(Inode& inode, int pos) {
  seekToInode(pos);
  inode.deserialize(disk);
}

//Seek to the position of a given data block
void BasicFS::seekToDataBlock(int pos) {
  throw std::runtime_error("BasicFS::seekToDataBlock not implemented yet!");
}

//Write a given dentry to a data block
void BasicFS::writeDentry(int pos) {
  throw std::runtime_error("BasicFS::writeDentry not implemented yet!");
}

//Read a dentry from a given data block
void BasicFS::readDentry(int pos) {
  throw std::runtime_error("BasicFS::readDentry not implemented yet!");
}


//Basic getter of inode object/entry from inode number
BasicFS::Inode BasicFS::getInodeByNumber(int inode_num) {
  //Find the beginning of the inode entries
  size_t inode_region_start = supblk->BLOCK_START_INODES *
                              Disk::blocksize_in_bytes;
  size_t inode_internal_offset = inode_num *
                                 (Disk::blocksize_in_bytes / INODES_PER_BLOCK);
  size_t inode_total_offset = inode_region_start + inode_internal_offset;

  //Retrieve inode
  disk.seek_to(inode_total_offset);
  Inode inode;
  inode.deserialize(disk);
}

//Returns inode number of the file/directory string, or -1 if not found
int BasicFS::getInodeFromPath(std::string path) {
  int cur_inode;
  Dentry cur_dentry;

  std::vector<std::string> path_elems = tokenize(path, pathSep());
  for (int i = 0; i < path_elems.size(); ++i) {
    std::cout << i << ": '" << path_elems[i] << "'" << std::endl;
  }

  for (int i = 0; i < path_elems.size(); ++i) {
    if (i == 0) {
      if (path_elems[0] == "") {
        //Absolute path
        cur_inode = supblk->root_inode;
        std::cout << "root inode at " << cur_inode << std::endl;
      }
      else {
        throw std::runtime_error("Can't handle relative paths yet!");
      }
    }
    //Look up path element in current Dentry
    //cur_dentry = getDentryByInode(cur_inode);
    //Inode cur_dentry.
  }
  throw std::runtime_error("getInodeFromPath not finished yet!");
  return -1;
}


// - BasicFS::Inode methods -

BasicFS::Inode::Inode() :
 data_block_num(-1) {}

BasicFS::Inode::Inode(int dbn) :
 data_block_num(dbn) {}

//Get the data block number
int BasicFS::Inode::getDBN() {
  return data_block_num;
}

//Get the offset from the beginning of the data blocks
size_t BasicFS::Inode::getAddress() {
  return data_block_num * Disk::blocksize_in_bytes;
}

//Write this object to disk
void BasicFS::Inode::serialize(Disk& disk) const {
  disk.writeInt(data_block_num);
}

//Read this object from disk
void BasicFS::Inode::deserialize(Disk& disk) {
  disk.readInt(data_block_num);
}

std::ostream& operator<<(std::ostream& os, BasicFS::Inode& ind) {
  os << "Inode(" << ind.data_block_num << ", addr:"
     << ind.getAddress() << ")";
  return os;
}
 
// - BasicFS::Dentry methods -

BasicFS::Dentry::Dentry() :
  num_entries_used(0) {}

//If we know the parents inode number, we can set '..' easier
BasicFS::Dentry::Dentry(int parent_inode_num, int my_inode_num) :
  num_entries_used(0) {
  addFile(".", my_inode_num);
  addFile("..", parent_inode_num);
}

//Write this object to disk
void BasicFS::Dentry::serialize(Disk& disk) const {
  disk.writeInt(num_entries_used);
  for (int i = 0; i < num_entries_used; ++i) {
    disk.writeChars(files[i].first, MAX_FILENAME_LEN);
    disk.writeInt(files[i].second);
  }
}

//Read this object from disk
void BasicFS::Dentry::deserialize(Disk& disk) {
  disk.readInt(num_entries_used);
  for (int i = 0; i < num_entries_used; ++i) {
    disk.readChars(files[i].first, MAX_FILENAME_LEN);
    disk.readInt(files[i].second);
  }
}


void BasicFS::Dentry::addFile(std::string fname, int inode_num) {
  if (num_entries_used >= MAX_NUM_FILES_PER_DIR-1)
    throw std::runtime_error("Can't add any more entries to this dentry!");
  strncpy(files[num_entries_used].first, fname.c_str(), MAX_FILENAME_LEN);
  files[num_entries_used].second = inode_num;
  num_entries_used += 1;
}

void BasicFS::Dentry::updateInode(std::string fname, int new_inode_num) {
  const char* fname_c{fname.c_str()};
  for (int i = 0; i < num_entries_used; ++i) {
    if (strncmp(fname_c, files[i].first, MAX_FILENAME_LEN) == 0) {
      files[i].second = new_inode_num;
      return;
    }
  }
  throw std::runtime_error("File not found");
}

//Only removes that entry from the dentry's list
//(Does not do any other filesystem maintenance)
void BasicFS::Dentry::removeFile(std::string fname) {
  throw std::runtime_error("BasicFS::Dentry::removeFile not implemented yet!");
}
 
std::ostream& operator<<(std::ostream& os, BasicFS::Dentry& de) {
  os << "Dentry(" << de.num_entries_used << "){";
  for (int i = 0; i < de.num_entries_used; ++i) {
    os << de.files[i].first << "->" << de.files[i].second;
    if (i < de.num_entries_used-1) os << ",";
  }
  os <<"}";
  return os;
}

// - BasicFS::IsUsedBitmap methods -

BasicFS::IsUsedBitmap::IsUsedBitmap(const int num_values)
  : size(num_values), bitmap(new char[num_values])
  {
  clear();  //Initialize to all 'cleared'
}

BasicFS::IsUsedBitmap::~IsUsedBitmap() {
  delete[] bitmap;
}

//Set all values to 'cleared'
void BasicFS::IsUsedBitmap::clear() {
  std::fill_n(bitmap, size, bitval_clr());
}

//Set a bit as given position
void BasicFS::IsUsedBitmap::set(unsigned int pos) {
  bitmap[pos] = bitval_set();
}

//Clear a bit at given position
void BasicFS::IsUsedBitmap::clr(unsigned int pos) {
  bitmap[pos] = bitval_clr();
}

//Test (return) the bit at given position
bool BasicFS::IsUsedBitmap::tst(unsigned int pos) {
  return (bitmap[pos] == bitval_set());
}
 
//Write this object to disk (assume that the fstream is in correct position)
void BasicFS::IsUsedBitmap::serialize(Disk& disk) const {
  disk.writeChars(bitmap, size);
}

//Read this object from disk (assume that the fstream is in correct position)
void BasicFS::IsUsedBitmap::deserialize(Disk& disk) {
  disk.readChars(bitmap, size);
}

std::ostream& operator<<(std::ostream& os, BasicFS::IsUsedBitmap& iub) {
  for (int i = 0; i < iub.size; ++i) {
    os << iub.bitmap[i];
    if ((i+1)%16 == 0) os << std::endl;
    else if ((i+1)%4 == 0) os << " ";
  }
  return os;
}

// - BasicFS::Superblock methods -

//Write this object to disk
void BasicFS::Superblock::serialize(Disk& disk) const {
  disk.writeInt(root_inode);
  disk.writeInt(BLOCK_COUNT_SUPERBLOCK);
  disk.writeInt(BLOCK_COUNT_INODE_BITMAP);
  disk.writeInt(BLOCK_COUNT_DATA_BITMAP);
  disk.writeInt(BLOCK_COUNT_INODES);
  disk.writeInt(BLOCK_COUNT_DATA);
  disk.writeInt(BLOCK_START_SUPERBLOCK);
  disk.writeInt(BLOCK_START_INODE_BITMAP);
  disk.writeInt(BLOCK_START_DATA_BITMAP);
  disk.writeInt(BLOCK_START_INODES);
  disk.writeInt(BLOCK_START_DATA);
}

//Read this object from disk
void BasicFS::Superblock::deserialize(Disk& disk) {
  /*
  disk.readInt(root_inode);
  disk.readInt(BLOCK_COUNT_SUPERBLOCK);
  disk.readInt(BLOCK_COUNT_INODE_BITMAP);
  disk.readInt(BLOCK_COUNT_DATA_BITMAP);
  disk.readInt(BLOCK_COUNT_INODES);
  disk.readInt(BLOCK_COUNT_DATA);
  disk.readInt(BLOCK_START_SUPERBLOCK);
  disk.readInt(BLOCK_START_INODE_BITMAP);
  disk.readInt(BLOCK_START_DATA_BITMAP);
  disk.readInt(BLOCK_START_INODES);
  disk.readInt(BLOCK_START_DATA);
  */
}

BasicFS::Superblock::Superblock() {}

BasicFS::Superblock::Superblock(int root_inode_,
                                int count_superblock,
                                int count_inode_bitmap,
                                int count_data_bitmap,
                                int count_inodes,
                                int count_data,
                                int start_superblock,
                                int start_inode_bitmap,
                                int start_data_bitmap,
                                int start_inodes,
                                int start_data) :
  root_inode(root_inode_),
  BLOCK_COUNT_SUPERBLOCK(count_superblock),
  BLOCK_COUNT_INODE_BITMAP(count_inode_bitmap),
  BLOCK_COUNT_DATA_BITMAP(count_data_bitmap),
  BLOCK_COUNT_INODES(count_inodes),
  BLOCK_COUNT_DATA(count_data),
  BLOCK_START_SUPERBLOCK(start_superblock),
  BLOCK_START_INODE_BITMAP(start_inode_bitmap),
  BLOCK_START_DATA_BITMAP(start_data_bitmap),
  BLOCK_START_INODES(start_inodes),
  BLOCK_START_DATA(start_data) {}
/* Test functions */

void test_Disk_backup_clear_and_restore() {

  Disk disk;

  std::cout << "Creating backup of the disk..." << std::endl;
  disk.create_backup(Disk::backup_filename);
  std::cout << "Done creating backup (to '"
            << Disk::backup_filename << "')" << std::endl;

  std::cout << "Clearing the disk..." << std::endl;
  disk.clear();
  std::cout << "Done clearing the disk" << std::endl;

  //TODO: Verify the clear?

  std::cout << "Restoring backup of the disk (from '"
            << Disk::backup_filename << "')" << std::endl;
  disk.restore_backup(Disk::backup_filename);
  std::cout << "Done restoring backup" << std::endl;
}

void test_Disk_writeInt_readInt() {
  int num = 12345;
  int int_pos = 1;
  int char_pos1 = 0;
  int char_pos2 = 20;
  char c1 = 'X';
  char c2 = 'Q';

  Disk disk;

  //Write numbers to the disk
  std::cout << "Writing " << num << " to location " << int_pos << std::endl;
  disk.writeInt(num, int_pos);

  std::cout << "Writing '" << c1 << "' to location " << char_pos1 << std::endl;
  disk.writeChar(c1, char_pos1);
  std::cout << "Writing '" << c2 << "' to location " << char_pos2 << std::endl;
  disk.writeChar(c2, char_pos2);

  //Read the values back to verify

  int read_num;
  char read_c1, read_c2;
  disk.readInt(read_num, int_pos);
  disk.readChar(read_c1, char_pos1);
  disk.readChar(read_c2, char_pos2);

  std::cout << "Read number: '" << read_num
            << "' from pos " << int_pos << std::endl;
  std::cout << "Read character: '" << read_c1
            << "' from pos " << char_pos1 << std::endl;
  std::cout << "Read character: '" << read_c2
            << "' from pos " << char_pos2 << std::endl;
}

void test_Disk_write_read_chars() {
  const int datasize = 28;
  char data[datasize];  //Buffer to write out
  char data2[datasize]; //Buffer to read into
  int pos = 40;

  //Put in some interesting values
  std::fill_n(data, datasize, 'x');
  data[0] = 'S';
  data[4] = 'A';
  data[10] = 'B';
  data[27] = 'Z';

  Disk disk;

  std::cout << "Data '" << data << "'" << std::endl;
  disk.writeChars(data, pos, datasize);

  disk.readChars(data2, pos, datasize);
  std::cout << "Read data: '" << data2 << "'" << std::endl;
}

void test_SystemDemo() {
  SystemDemo sd;
  int pos1 = 100;
  int pos2 = 200;

  Disk disk;

  sd.public_int_val = 345;
  sd.public_char_val = 'r';

  disk.seek_to(pos1);
  sd.serialize(disk);

  disk.seek_to(pos2);
  sd.serialize(disk);

  SystemDemo sd2;

  std::cout << "Original (default values) SystemDemo object sd2:" << std::endl;
  std::cout << sd2;

  std::cout << "\nReading into sd2, from pos " << pos1 << std::endl;
  disk.seek_to(pos1);
  sd2.deserialize(disk);

  std::cout << "Loaded SystemDemo object sd2:" << std::endl;
  std::cout << sd2;
}

void test_Inode() {
  Disk disk;
  BasicFS bfs(disk);

  BasicFS::Inode inode(3);
  bfs.writeInode(inode, 5);

  BasicFS::Inode inode2;
  bfs.readInode(inode2, 5);

  std::cout << inode2 << std::endl;
}

void test_Dentry() {
  Disk disk;
  BasicFS::Dentry dentry(2, 3); //Parent and "my" inode numbers
  dentry.addFile("luke.txt", 10);
  dentry.addFile("luke2.dat", 12);
  std::cout << dentry << std::endl;
  dentry.updateInode("luke2.dat", 15);

  disk.seek_to(30);
  dentry.serialize(disk);

  BasicFS::Dentry dentry2;
  disk.seek_to(30);
  dentry2.deserialize(disk);
  std::cout << dentry2 << std::endl;
}

void test_BasicFS() {
  Disk disk;

  //bfs.getInodeFromPath("/home/luke/test.txt");
}

void test_BasicFS_IsUsedBitmap() {
  Disk disk;
  BasicFS::IsUsedBitmap bm1(20);

  bm1.set(2);
  bm1.set(12);
  bm1.set(19);

  if (bm1.tst(2)) std::cout << "bit 2 is set" << std::endl;
  else std::cout << "bit 2 is not set" << std::endl;
  if (bm1.tst(3)) std::cout << "bit 3 is set" << std::endl;
  else std::cout << "bit 3 is not set" << std::endl;

  std::cout << bm1 << std::endl;

}

int main() {

  Disk::create();

  //test_Disk_backup_clear_and_restore();
  //test_Disk_writeInt_readInt();
  //test_Disk_write_read_chars();
  //test_SystemDemo();
  test_Inode();
  test_Dentry();
  test_BasicFS_IsUsedBitmap();
  //test_BasicFS();

  return 0;
}
