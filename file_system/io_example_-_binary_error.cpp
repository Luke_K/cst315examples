#include <iostream>
#include <fstream>
#include <string>


int main() {
  std::cout << "Starting" << std::endl;
  std::cout << "'out':" << std::ios::out << std::endl;
  std::cout << "'binary':" << std::ios::binary << std::endl;
  std::cout << "both:" << (std::ios::binary|std::ios::out) << std::endl;
  std::string fname = "filesys.dat";
  std::fstream f(fname, std::ios::out|std::ios::binary);
  int x = 5;
  std::string s = "This is a test";
  float y = 3.14;
  
  f.write((char*)&x, sizeof(x));

  //int sz = sizeof(s);
  //std::cout << "size of s: " << sz << std::endl;
  //f.write((char*)&sz, sizeof(sz));
  //f.write((char*)&s, sizeof(s));
  f << s;

  f.write((char*)&y, sizeof(y));

  f.flush();
  f.close();

  std::fstream f2(fname, std::ios::in|std::ios::binary);

  int x2;
  std::string s2;
  float y2;
  f2.read((char*)&x2, sizeof(x2));
  //f2.read((char*)&sz, sizeof(sz));
  //f2.read((char*)&s2, sz);
  f2 >> s2;
  f2.read((char*)&y2, sizeof(y2));
  f2.close();

  std::cout << "x2: " << x2 << std::endl;
  std::cout << "s2: " << s2 << std::endl;
  std::cout << "y2: " << y2 << std::endl;

  std::cout << "Done" << std::endl;

  return 0;
};
