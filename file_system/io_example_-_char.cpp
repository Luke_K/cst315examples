/*******************************************************************************
* File: io_example_-_char.cpp
* Author: Luke Kanuchok
* Date: 2018/04/12
*
* Notes: Use the static disk method 'Disk::create()' to create the disk.
*
* Requires C++11 flag: "g++ -std=c++11 io_example_char.cpp"
*******************************************************************************/

#include <iostream>
#include <fstream>
#include <iomanip> //For std::setfill
#include <sstream> //For reading integers

#include <sys/stat.h>  //For stat
#include <fcntl.h>  //For open
#include <sys/types.h> //For open flags
#include <unistd.h>  //For write

const int filesize = 1024;

class Disk {
  public:
  Disk();
  ~Disk();

  //Connect (open file system disk file), and prepare for operation
  void start();

  //Finish up all writes, and disconnect from the disk
  void finalize();
  
  //Write all zeroes to the disk
  void clear();

  //Make a backup of the disk
  void create_backup(std::string backup_filename);

  //Re-load the backup
  void restore_backup(std::string backup_filename);

  //Check for drive file existence
  static bool drive_exists(const std::string drive_filename);

  //Destroy the drive file (remove the disk file)
  static void destroy(const std::string drive_filename);

  //Create a drive (tests for existence, and silently returns if disk exists)
  static void create(const std::string drive_filename);

  //Set the disk location
  void seek_to(const int location);

  //Write an integer to a given location/address on "disk"
  void writeInt(const int data, const long address);

  //Read an integer from a given address on "disk"
  void readInt(int& data, const int address);

  //Write a single character out to a file, at a given address
  void writeChar(const char data, const int address);

  //Read a character/byte from a given address on "disk"
  void readChar(char& data, const int address);

  //Write an array of characters out to a file, at a given address
  void writeChars(const char* data, const int count, const int address);

  //Read a character/byte from a given address on "disk"
  void readChars(char* data, const int count, const int address);

  const static std::string filesystem_filename;
  const static std::string backup_filename;


  private:
  //The disk for supporting our file system
  const std::ios_base::openmode accessflags;

  const static size_t blocksize_in_bytes = 4096;
  const static size_t disksize_in_blocks = 32;
  const static size_t disksize_in_bytes = disksize_in_blocks *
                                          blocksize_in_bytes;

  //Our access to the disk
  std::fstream fs;
};
const std::string Disk::filesystem_filename = "filesys.dat";
const std::string Disk::backup_filename = "filesys.bkup";

//Abstract base class for structures to be stored on disk
class SystemDataStructure {
  public:

  //Write this object to disk
  virtual void serialize(Disk& disk) = 0;
  //Read this object from disk
  virtual void deserialize(Disk& disk) = 0;
  protected:
  private:
};

//Demonstrate the use of SystemDataStructure
const int PUBLIC_CHAR_ARR_SIZE = 6;
const int PRIVATE_CHAR_ARR_SIZE = 5;
class SystemDemo : public SystemDataStructure {
  public:
  SystemDemo();

  //Write this object to disk
  void serialize(Disk& disk);
  //Read this object from disk
  void deserialize(Disk& disk);

  friend std::ostream& operator<<(std::ostream& os, SystemDemo& sd);

  int public_int_val;
  char public_char_val;
  char public_char_arr[PUBLIC_CHAR_ARR_SIZE];
  protected:
  private:
  int private_int_val;
  char private_char_val;
  char private_char_arr[PRIVATE_CHAR_ARR_SIZE];
};

// - Disk methods -

Disk::Disk()
  : accessflags(std::ios::in|std::ios::out) {
  start();
}

Disk::~Disk() {
  finalize();
}


void Disk::start() {
  fs.open(filesystem_filename, accessflags);
  std::cout << "Disk opened, system started" << std::endl;
}

void Disk::finalize() {
  fs.close();
  std::cout << "Disk closed, system halted" << std::endl;
}

void Disk::clear() {
  //Save the current location
  long saved_pos = fs.tellp();

  //Go to the beginning of the disk
  fs.seekp(0);  //Seek for (out)Put operation 
  for (int i = 0; i < disksize_in_bytes; ++i)
    fs.put(0);

  //Go back to where we were
  fs.seekp(saved_pos);
}

//Make a backup of the disk
void Disk::create_backup(std::string backup_filename) {
  std::ofstream bkup(backup_filename);
  //Save the current location
  long saved_pos = fs.tellp();

  //Go to the beginning of the disk
  fs.seekp(0);  //Seek for (out)Put operation 
  bkup << fs.rdbuf();

  bkup.close();

  //Go back to where we were
  fs.seekp(saved_pos);
}

//Set the disk location
void Disk::seek_to(const int location) {
  fs.seekg(location);
}

//Re-load the backup
void Disk::restore_backup(std::string backup_filename) {
  std::ifstream bkup(backup_filename);
  //Save the current location
  long saved_pos = fs.tellp();

  //Go to the beginning of the disk
  fs.seekp(0);  //Seek for (out)Put operation 
  fs << bkup.rdbuf();

  bkup.close();

  //Go back to where we were
  fs.seekp(saved_pos);
}

//Check for drive file existence
bool Disk::drive_exists(const std::string drive_filename) {
  struct stat buffer;
  return (stat (drive_filename.c_str(), &buffer) == 0);
}

//Destroy the drive file (remove the disk file)
void Disk::destroy(const std::string drive_filename =
                          Disk::filesystem_filename) {
  if (Disk::drive_exists(drive_filename))
    remove(drive_filename.c_str());
}

//Create a drive (tests for existence, and silently returns if disk exists)
void Disk::create(const std::string drive_filename =
                         Disk::filesystem_filename) {
  if (!Disk::drive_exists(drive_filename)) {
    //Low level disk creation
    char blk_buf[Disk::blocksize_in_bytes];
    std::fill_n(blk_buf, Disk::blocksize_in_bytes, 0);
    int fd = open(drive_filename.c_str(), O_CREAT|O_WRONLY, S_IRUSR|S_IWUSR);
    for (int i = 0; i < Disk::disksize_in_blocks; ++i)
      write(fd, blk_buf, Disk::blocksize_in_bytes);
    close(fd);
  }
}

//Write an integer to a given location/address on "disk"
//If the address is less than zero (default), then write
//to the current position.
//Zero padded to 10 characters, right justified, as we may
//Want to store large numbers in a fixed field size
void Disk::writeInt(const int data, const long address = -1) {
  //fmt flags code adapted from:
  //https://stackoverflow.com/a/2273352
  std::ios::fmtflags saved_format(fs.flags());
  if (address >= 0) //Seek to a new position
    fs.seekp(address);

  fs.fill('0');
  fs.width(10);
  fs << data;

  fs.flags(saved_format);
}

//Read an integer from a given address on "disk"
//If the address is less than zero (default), then read
//from the current position
void Disk::readInt(int& data, const int address = -1) {
  if (address >= 0) //Seek to a new position
    fs.seekg(address);

  char buf[10];
  fs.read(buf, 10);
  std::stringstream ss(buf);
  ss >> data;
}

//Write a single character out to a file, at a given address
void Disk::writeChar(const char data, const int address = -1) {
  if (address >= 0) //Seek to a new position
    fs.seekg(address);
  
  fs << data;
}

//Read a character/byte from a given address on "disk"
void Disk::readChar(char& data, const int address = -1) {
  if (address >= 0) //Seek to a new position
    fs.seekg(address);
  
  fs >> data;
}

//Write an array of characters out to a file, at a given address
void Disk::writeChars(const char* data, const int count, const int address = -1) {
  if (address >= 0) //Seek to a new position
    fs.seekp(address);

  fs.write(data, count);
}

//Read a character/byte from a given address on "disk"
void Disk::readChars(char* data, const int count, const int address = -1) {
  if (address >= 0) //Seek to a new position
    fs.seekg(address);
  fs.read(data, count);
}

// - SystemDemo methods -
SystemDemo::SystemDemo()
  : public_int_val(42),
    public_char_val('x'),
    public_char_arr{'a','b','c','d','e'},
    private_int_val(200),
    private_char_val('f'),
    private_char_arr{'w','x','y','z'}
    {}

//It is assumed that the disk file stream has been put to the corect position
void SystemDemo::serialize(Disk& disk) {
  disk.writeInt(public_int_val);
  disk.writeChar(public_char_val);
  disk.writeChars(public_char_arr, PUBLIC_CHAR_ARR_SIZE);
  disk.writeInt(private_int_val);
  disk.writeChar(private_char_val);
  disk.writeChars(private_char_arr, PRIVATE_CHAR_ARR_SIZE);
}

//It is assumed that the disk file stream has been put to the corect position
void SystemDemo::deserialize(Disk& disk) {
  disk.readInt(public_int_val);
  disk.readChar(public_char_val);
  disk.readChars(public_char_arr, PUBLIC_CHAR_ARR_SIZE);
  disk.readInt(private_int_val);
  disk.readChar(private_char_val);
  disk.readChars(private_char_arr, PRIVATE_CHAR_ARR_SIZE);
}

std::ostream& operator<<(std::ostream& os, SystemDemo& sd) {
  os << "public int val: " << sd.public_int_val << std::endl;
  os << "public char val: " << sd.public_char_val << std::endl;
  os << "public char arr: " << sd.public_char_arr << std::endl;
  os << "private int val: " << sd.private_int_val << std::endl;
  os << "private char val: " << sd.private_char_val << std::endl;
  os << "private char arr: " << sd.private_char_arr << std::endl;
  return os;
}

/* Test functions */
void test_Disk_backup_clear_and_restore() {

  Disk disk;

  std::cout << "Creating backup of the disk..." << std::endl;
  disk.create_backup(Disk::backup_filename);
  std::cout << "Done creating backup (to '"
            << Disk::backup_filename << "')" << std::endl;

  std::cout << "Clearing the disk..." << std::endl;
  disk.clear();
  std::cout << "Done clearing the disk" << std::endl;

  //TODO: Verify the clear?

  std::cout << "Restoring backup of the disk (from '"
            << Disk::backup_filename << "')" << std::endl;
  disk.restore_backup(Disk::backup_filename);
  std::cout << "Done restoring backup" << std::endl;
}

void test_Disk_writeInt_readInt() {
  int num = 12345;
  int int_pos = 1;
  int char_pos1 = 0;
  int char_pos2 = 20;
  char c1 = 'X';
  char c2 = 'Q';

  Disk disk;

  //Write numbers to the disk
  std::cout << "Writing " << num << " to location " << int_pos << std::endl;
  disk.writeInt(num, int_pos);

  std::cout << "Writing '" << c1 << "' to location " << char_pos1 << std::endl;
  disk.writeChar(c1, char_pos1);
  std::cout << "Writing '" << c2 << "' to location " << char_pos2 << std::endl;
  disk.writeChar(c2, char_pos2);

  //Read the values back to verify

  int read_num;
  char read_c1, read_c2;
  disk.readInt(read_num, int_pos);
  disk.readChar(read_c1, char_pos1);
  disk.readChar(read_c2, char_pos2);

  std::cout << "Read number: '" << read_num
            << "' from pos " << int_pos << std::endl;
  std::cout << "Read character: '" << read_c1
            << "' from pos " << char_pos1 << std::endl;
  std::cout << "Read character: '" << read_c2
            << "' from pos " << char_pos2 << std::endl;
}

void test_Disk_write_read_chars() {
  const int datasize = 28;
  char data[datasize];  //Buffer to write out
  char data2[datasize]; //Buffer to read into
  int pos = 40;

  //Put in some interesting values
  std::fill_n(data, datasize, 'x');
  data[0] = 'S';
  data[4] = 'A';
  data[10] = 'B';
  data[27] = 'Z';

  Disk disk;

  std::cout << "Data '" << data << "'" << std::endl;
  disk.writeChars(data, pos, datasize);

  disk.readChars(data2, pos, datasize);
  std::cout << "Read data: '" << data2 << "'" << std::endl;
}

void test_SystemDemo() {
  SystemDemo sd;
  int pos1 = 100;
  int pos2 = 200;

  Disk disk;

  sd.public_int_val = 345;
  sd.public_char_val = 'r';

  disk.seek_to(pos1);
  sd.serialize(disk);

  disk.seek_to(pos2);
  sd.serialize(disk);

  SystemDemo sd2;

  std::cout << "Original (default values) SystemDemo object sd2:" << std::endl;
  std::cout << sd2;

  std::cout << "\nReading into sd2, from pos " << pos1 << std::endl;
  disk.seek_to(pos1);
  sd2.deserialize(disk);

  std::cout << "Loaded SystemDemo object sd2:" << std::endl;
  std::cout << sd2;
}

int main() {

  Disk::create();

  //test_Disk_backup_clear_and_restore();
  //test_Disk_writeInt_readInt();
  //test_Disk_write_read_chars();
  test_SystemDemo();

  return 0;
}
