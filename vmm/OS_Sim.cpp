/***************************************************************************
* Filename: OS_Sim.cpp
* Author: Luke Kanuchok
* Date: 2018/03/11
*
* Class: OS_Sim
*
* Class components:
***************************************************************************/

#include "OS_Sim.h"
#include <iostream>

using std::cout;
using std::endl;
using std::hex;

OS_Sim::OS_Sim() {
}

void OS_Sim::LoadDataToMemory(int diskOffset, int memoryOffset, int size) {
  cout << "OS: Loading " << size << " bytes of memory..." << endl;
  cout << "  from disk loc(" << hex << diskOffset << ")" << endl;
  cout << "  to memory loc(" << hex << memoryOffset << ")" << endl;
}

void OS_Sim::StoreDataToDisk(int memoryOffset, int swapOffset, int size) {
  cout << "OS: Storing " << size << " bytes of memory..." << endl;
  cout << "  from memory loc(" << hex << memoryOffset << ")" << endl;
  cout << "  to swap loc(" << hex << swapOffset << ")" << endl;
}

