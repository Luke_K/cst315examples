#ifndef _OS_SIM_H_
#define _OS_SIM_H_

/***************************************************************************
* Filename: OS_Sim.cpp
* Author: Luke Kanuchok
* Date: 2018/03/11
*
* Class: OS_Sim
*
* Class components:
*   Public data members:
*    - NONE
*   Public methods:
*    - NONE
*   Protected data members:
*    - NONE
*   Protected methods:
*    - NONE
*   Private data members:
*    - NONE
*   Private methods:
*    - NONE

* Example data member
*    - Name:
*      - Type: 
*      - Intent: 

* Example method
*    - Name: 
*      - Return type: 
*      - Arguments:
*        - Name: 
*        - Type: 
*        - Intent: 
*      - Intent: 
***************************************************************************/

class OS_Sim {
  public:
  //- Public data members -

  //- Public methods
  OS_Sim();

  void LoadDataToMemory(int diskOffset, int memoryOffset, int size);

  void StoreDataToDisk(int memoryOffset, int swapOffset, int size);

  protected:
  //- Protected data members -

  //- Protected methods

  private:
  //- Private data members -

  //- Private methods
  
};

#endif
