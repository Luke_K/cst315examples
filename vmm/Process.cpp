/***************************************************************************
* Filename: Process.cpp
* Author: Luke Kanuchok
* Date: 2018/03/11
*
* Class: Process
*
* Class components:
***************************************************************************/

#include "Process.h"

#include <iostream> //For std::hex
#include <fstream> //For std::ifstream
#include <locale> //for std::locale
#include <cassert> //For std::assert
#include <string>  //For std::compare
#include "reader.h"
#include "Segment.h"

using std::cout;
using std::endl;

Process::Process(string proc_data_filename,
                 int process_id) :
                 pid(process_id),
                 p_VMM(nullptr),
                 stat_id(NEW),
                 ip(0) {
  create_segments(proc_data_filename);
}

void Process::create_segments(string fname) {
  using std::hex;

  ifstream in_file(fname);
  string ln, label;
  int code_begin, code_end, data_begin, data_end, code_lines, data_lines;

  std::locale c_r(std::locale::classic(), new ColonReader);
  in_file.imbue(c_r);

  //Get code begin
  in_file >> label >> hex >> code_begin;
  assert(label.compare("CODEBEGIN") == 0 && "Wrong line, expected CODEBEGIN");

  //Get code end
  in_file >> label >> code_end;
  assert(label.compare("CODEEND") == 0 && "Wrong line, expected CODEEND");

  //Get data begin
  in_file >> label >> data_begin;
  assert(label.compare("DATABEGIN") == 0 && "Wrong line, expected DATABEGIN");

  //Get data end
  in_file >> label >> data_end;
  assert(label.compare("DATAEND") == 0 && "Wrong line, expected DATAEND");
  
  //Get code lines
  in_file >> label >> code_lines;
  assert(label.compare("CODELINES") == 0 && "Wrong line, expected CODELINES");

  //Get data lines
  in_file >> label >> data_lines;
  assert(label.compare("DATALINES") == 0 && "Wrong line, expected DATALINES");

  seg_code = new Segment("CODE", code_begin, code_end, code_lines, in_file);
  seg_data = new Segment("DATA", data_begin, data_end, data_lines, in_file);
}

void Process::connectTo(VMM* p) {
  p_VMM = p;
}

void Process::run_instruction() {
  //Run an instruction (printing status for debugging)
  cout << "Process " << pid << " running instuction " << ip;
  cout << "(" << stat_id << ")" << endl;

  //TODO: Do stuff

  //Increment instruction pointer
  ++ip;

  //TODO: Update stat_id as necessary

  //Are we done?
  if (ip > seg_code->num_lines) {
    stat_id = FINISHED;
  }
}

ostream& operator<<(ostream& os, Process& proc) {
  os << "Process(pid:" << proc.pid << ",\n";
  os << "  " << *proc.seg_code << "\n";
  os << "  " << *proc.seg_data << "\n)";
  return os;
}

