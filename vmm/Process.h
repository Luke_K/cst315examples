#ifndef _PROCESS_H_
#define _PROCESS_H_

/***************************************************************************
* Filename: Process.cpp
* Author: Luke Kanuchok
* Date: 2018/03/11
*
* Class: Process
*
* Class components:
*   Public data members:
*    - NONE
*   Public methods:
*    - NONE
*   Protected data members:
*    - NONE
*   Protected methods:
*    - NONE
*   Private data members:
*    - NONE
*   Private methods:
*    - NONE

* Example data member
*    - Name:
*      - Type: 
*      - Intent: 

* Example method
*    - Name: 
*      - Return type: 
*      - Arguments:
*        - Name: 
*        - Type: 
*        - Intent: 
*      - Intent: 
***************************************************************************/

#include <string>
#include "Segment.h"
#include "VMM.h"

using std::string;

enum StatusID {NEW, READY, BLOCKED, FINISHED};

class Process {
  public:

  //- Public data members -
  const string proc_name;
  const int pid;

  StatusID stat_id; //TODO: Left in public for simpliciy

  //- Public methods
  Process(string data_fname, int process_id); //Parameterized constructor

  void connectTo(VMM* p_VMM);

  void run_instruction();

  protected:
  //- Protected data members -

  //- Protected methods

  private:
  //- Private data members -
  Segment* seg_code;
  Segment* seg_data;

  VMM* p_VMM;

  //Instruction pointer: which is the 'next' instruction?
  int ip;

  //- Private methods
  void create_segments(string fname);

  friend ostream& operator<<(ostream& os, Process& proc);
};

#endif
