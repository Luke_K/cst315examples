/***************************************************************************
* Filename: Segment.cpp
* Author: Luke Kanuchok
* Date: 2018/03/11
*
* Class: Segment
*
* Class components:
***************************************************************************/

#include "Segment.h"

#include <iostream>   //For debugging

using std::hex;
using std::dec;

Segment::Segment(string seg_name,
                 int seg_begin,
                 int seg_end,
                 int line_count,
                 ifstream& ifs
                 ) :
                 begin(seg_begin),
                 end(seg_end),
                 num_lines(line_count),
                 name(seg_name) {
  load_data(ifs);
}

void Segment::load_data(ifstream& ifs) {
  string op;
  int arg;
  for (int i = 0; i < num_lines; ++i) {
    ifs >> op >> hex >> arg;
    data.push_back(pair<string, int>{op, arg});
  }
}

ostream& operator<<(ostream& os, Segment& seg) {
  os << "Segment(\"" << seg.name << "\", begin:0x" << hex << seg.begin;
  os << ", end:0x" << seg.end << ", lines:" << dec << seg.num_lines << ")";
  return os;
}
