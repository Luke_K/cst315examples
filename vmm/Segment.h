#ifndef _SEGMENT_H_
#define _SEGMENT_H_

/***************************************************************************
* Filename: Segment.h
* Author: Luke Kanuchok
* Date: 2018/03/11
*
* Class: 
*
* Class components:
*   Public data members:
*    - NONE
*   Public methods:
*    - NONE
*   Protected data members:
*    - NONE
*   Protected methods:
*    - NONE
*   Private data members:
*    - NONE
*   Private methods:
*    - NONE

* Example data member
*    - Name:
*      - Type: 
*      - Intent: 

* Example method
*    - Name: 
*      - Return type: 
*      - Arguments:
*        - Name: 
*        - Type: 
*        - Intent: 
*      - Intent: 
***************************************************************************/

#include <string> //For std::string
#include <fstream>  //for std::ifstream, std::ostream
#include <vector>  //For std::vector
#include <utility> //For std::pair

using std::string;
using std::ifstream;
using std::ostream;
using std::vector;
using std::pair;

class Segment {
  public:
  //- Public data members -
  const int begin;
  const int end;
  const int num_lines;
  const string name;

  //- Public methods
  Segment(string seg_name,
          int seg_begin,
          int seg_end,
          int line_count,
          ifstream& ifs);

  protected:
  //- Protected data members -

  //- Protected methods

  private:
  //- Private data members -
  vector<pair<string, int>> data;

  //- Private methods
  void load_data(ifstream& ifs);

  friend ostream& operator<<(ostream& os, Segment& seg);

};

#endif
