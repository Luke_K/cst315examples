/***************************************************************************
* Filename: VMM.cpp
* Author: Luke Kanuchok
* Date: 2018/03/11
*
* Class: VMM
*
* Class components:
***************************************************************************/

#include "VMM.h"
#include "OS_Sim.h"

VMM::VMM() {}

VMM::VMM(OS_Sim* const p_to_os) : p_os(p_to_os) {}

void VMM::connectTo(OS_Sim* const p_to_OS) {
  p_os = p_to_OS;
}
