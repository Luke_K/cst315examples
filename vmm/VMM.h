#ifndef _VMM_H_
#define _VMM_H_

/***************************************************************************
* Filename: VMM.h
* Author: Luke Kanuchok
* Date: 2018/03/11
*
* Class: VMM
*
* Class components:
*   Public data members:
*    - NONE
*   Public methods:
*    - NONE
*   Protected data members:
*    - NONE
*   Protected methods:
*    - NONE
*   Private data members:
*    - NONE
*   Private methods:
*    - NONE

* Example data member
*    - Name:
*      - Type: 
*      - Intent: 

* Example method
*    - Name: 
*      - Return type: 
*      - Arguments:
*        - Name: 
*        - Type: 
*        - Intent: 
*      - Intent: 
***************************************************************************/

#include "OS_Sim.h"

class VMM {
  public:
  //- Public data members -

  //- Public methods
  VMM();
  VMM(OS_Sim* const p_to_op);

  //Prints activity to the screen!
  void xlate_virtual_to_physical(const int& virt_addr, int& phys_addr);

  void connectTo(OS_Sim* const p_to_OS);

  protected:
  //- Protected data members -

  //- Protected methods

  private:
  //- Private data members -
  OS_Sim* p_os;

  //- Private methods

};

#endif
