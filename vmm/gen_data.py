#!/usr/bin/env python3

lbl_code_begin = "CODEBEGIN"
lbl_code_end   = "CODEEND"
lbl_data_begin = "DATABEGIN"
lbl_data_end   = "DATAEND"
lbl_code_lines = "CODELINES"
lbl_data_lines = "DATALINES"
header_divider = ":"
instruction_divider = " "
data_divider = " "

code_instructions = ["READ", "WRTE"]

data_valuetypes = ["CONST", "ADDR"]

script_instructions = \
  "generate a set of data for use with the CST315 project 2 simulator"

addr_fmt = hex  # Formatter for address output

# Legal range for constants will be 0x00000001 through 0xFFFFFFFF, inclusive
const_range_begin = 0x00000001
const_range_end   = 0xFFFFFFFF + 1

default_code_begin_address = 0x01000000
default_code_end_address   = 0x03FFFFFF
default_data_begin_address = 0x05000000
default_data_end_address   = 0x07FFFFFF
default_num_code_lines     = 100
default_num_data_lines     = 200

# gen_process generates a single data set for input into our simulator
# The arguments are:
# - code_begin_addr: The first legal address in the code region
# - code_end_addr: The first address after the code region (value is excluded)
# - data_begin_addr: The first legal address in the data region
# - data_end_addr: The first address after the data region (value is excluded)
# - num_code_lines: The count of lines in the code portion
# - num_data_lines: The count of lines in the data portion
# - allow_illegal_addrs: Boolean controlling if addresses outside the legal
#                        region will be included (to cause faults)
#
# returns a string of lines joined by newlines for writing to a file

def gen_process(code_begin_addr,           # Integer
                code_end_addr,             # Integer
                data_begin_addr,           # Integer
                data_end_addr,             # Integer
                num_code_lines,            # Integer
                num_data_lines,            # Integer
                use_illegal_addrs=False):  # Boolean
  from random import choice, randrange

  # Construct list for lines
  result = []
  # Write labels with values
  result.append(lbl_code_begin + header_divider + addr_fmt(code_begin_addr))
  result.append(lbl_code_end   + header_divider + addr_fmt(code_end_addr))
  result.append(lbl_data_begin + header_divider + addr_fmt(data_begin_addr))
  result.append(lbl_data_end   + header_divider + addr_fmt(data_end_addr))
  result.append(lbl_code_lines + header_divider + addr_fmt(num_code_lines))
  result.append(lbl_data_lines + header_divider + addr_fmt(num_data_lines))
  
  # Write generated code lines:
  # - "instruction address", space separated
  # where instruction is one of
  #  - "READ"
  #  - "WRTE"
  # and address is a value taken from:
  # - hexadecimal range code_begin_addr - code_end_addr inclusive
  # - hexadecimal range data_begin_addr - data_end_addr inclusive
  for i in range(num_code_lines):
    code_op = choice(code_instructions)
    addr = randrange(code_begin_addr, code_end_addr)

    result.append(code_op + instruction_divider + addr_fmt(addr))

  # Write generated data lines
  # - "valuetype value", space separated

  # where valuetype is one of

  #  - "CONST"
  # and value taken from:
  # - hexadecimal range 0x00000001 - 0xFFFFFFFF, inclusive

  # OR

  #  - "ADDRS"
  # and value taken from:
  # - hexadecimal range code_begin_addr - code_end_addr inclusive
  # - hexadecimal range data_begin_addr - data_end_addr inclusive
  for i in range(num_data_lines):
    val_type = choice(data_valuetypes)
    value = None
    if val_type == "CONST":
      value = randrange(const_range_begin, const_range_end)
    elif val_type == "ADDR":
      range_choice = choice( [(code_begin_addr, code_end_addr),
                              (data_begin_addr, data_end_addr)] )
      value = randrange(*range_choice)

    result.append(val_type + data_divider + addr_fmt(value))

  return "\n".join(result)

def get_parser():
  from argparse import ArgumentParser as ap
  parser = ap(description=script_instructions)

  #From: https://stackoverflow.com/questions/25513043/
  #              python-argparse-fails-to-parse-hex-formatting-to-int-type
  def auto_int(x):
    return int(x, 0)

  # output filename
  parser.add_argument(
    'output_file',
    metavar='outputfilename',
    type=str,
    help='a filename to hold the data')
  
  # code_begin_addr
  parser.add_argument(
  '--code_begin_addr',  # name
  metavar='code beginning address', # name for generic data value
  nargs='?', # '?'=0or1, '*'=0ormore(list), '+'=1ormore(list)
  type=auto_int,  # python type, using helper function
  default=default_code_begin_address,
  help='the starting (included) value for the range of code')  # descriptive help string

  # code_end_addr
  parser.add_argument(
  '--code_end_addr',  # name
  metavar='code ending address', # name for generic data value
  nargs='?', # '?'=0or1, '*'=0ormore(list), '+'=1ormore(list)
  type=auto_int,  # python type, using helper function
  default=default_code_end_address,
  help='the final (excluded) value for the range of code')  # descriptive help string

  # data_begin_addr
  parser.add_argument(
  '--data_begin_addr',  # name
  metavar='data beginning address', # name for generic data value
  nargs='?', # '?'=0or1, '*'=0ormore(list), '+'=1ormore(list)
  type=auto_int,  # python type, using helper function
  default=default_data_begin_address,
  help='the starting (included) value for the range of data')  # descriptive help string

  # data_end_addr
  parser.add_argument(
  '--data_end_addr',  # name
  metavar='data ending address', # name for generic data value
  nargs='?', # '?'=0or1, '*'=0ormore(list), '+'=1ormore(list)
  type=auto_int,  # python type, using helper function
  default=default_data_end_address,
  help='the final (excluded) value for the range of data')  # descriptive help string

  # num_code_lines
  parser.add_argument(
  '--num_code_lines',  # name
  metavar='count generated code lines', # name for generic data value
  nargs='?', # '?'=0or1, '*'=0ormore(list), '+'=1ormore(list)
  type=int,  # python type
  default=default_num_code_lines,
  help='how many code lines to generate')  # descriptive help string

  # num_data_lines
  parser.add_argument(
  '--num_data_lines',  # name
  metavar='count generated data lines', # name for generic data value
  nargs='?', # '?'=0or1, '*'=0ormore(list), '+'=1ormore(list)
  type=int,  # python type
  default=default_num_data_lines,
  help='how many data lines to generate')  # descriptive help string

  # use_illegal_addrs
  parser.add_argument(
  '--use_illegal_addrs',  # name
  metavar='Use illegal addresses?', # name for generic data value
  nargs='?', # '?'=0or1, '*'=0ormore(list), '+'=1ormore(list)
  type=bool,  # python type
  default=False,
  help='should the data include faulty addresses')  # descriptive help string

  return parser


if __name__ == "__main__":
  # run as script
  from sys import argv
  parser = get_parser()
  args = parser.parse_args()
  #print(args)
  #exit(1)
  data = gen_process(args.code_begin_addr,
                     args.code_end_addr,
                     args.data_begin_addr,
                     args.data_end_addr,
                     args.num_code_lines,
                     args.num_data_lines,
                     args.use_illegal_addrs)
  f = open(args.output_file, 'w')
  f.write(data)
  f.close()

