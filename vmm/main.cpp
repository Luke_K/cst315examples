#include <iostream>
#include "Process.h"
#include "OS_Sim.h"
#include "VMM.h"
#include <queue>

using namespace std;

const int times_up = 5;

int main() {
  Process procs[3]{{"proc1.dat", 2},
                   {"proc2.dat", 3},
                   {"proc3.dat", 4}};

  OS_Sim os;
  VMM vmm(&os);

  vector<Process*> blocked;
  queue<Process*> ready;
  Process* running = nullptr;
  vector<Process*> finished;

  //Set up
  for (int i = 0; i < 3; ++i) {
    procs[i].connectTo(&vmm);
    procs[i].stat_id = READY;
    cout << procs[i] << endl;
    ready.push(&procs[i]);
  }

  bool still_running = true;
  //still_running = false;
  int timer = 0;

  while (still_running) {

    //TODO: Move blocked processes that are ready to ready queue

    //If nothings running, start next process
    if (running == nullptr && !ready.empty()) {
      running = ready.front();
      ready.pop();
    }
    else if (ready.empty() && blocked.empty()) {
      still_running = false; //We're done
      continue;
    }

    //If times up, remove running process
    if (timer >= times_up) {
      //Start the next process running
      ready.push(running);
      running = nullptr;
      timer = 0; //Reset
      cout << "Resetting..." << endl;
      continue;
    }

    timer++; //Increment time


    if (running != nullptr) {
      running->run_instruction();
      //TODO: Move blocked process from running to blocked
    }

    //If running process is finished or blocked, move to new place
    if (running->stat_id == BLOCKED) {
      blocked.push_back(running);
      running = nullptr;
    }
    else if (running->stat_id == FINISHED) {
      finished.push_back(running);
      cout << "Process " << running->pid << " finished." << endl;
      running = nullptr;
    }
  }

  return 0;
}
