//Reader from https://stackoverflow.com/questions/10376199/how-can-i-use-non-default-delimiters-when-reading-a-text-file-with-stdfstream

#include <algorithm>  //For copy_n

class ColonReader: public
std::ctype<char>
{
    mask my_table[table_size];
public:
    ColonReader(size_t refs = 0)  
        : std::ctype<char>(&my_table[0], false, refs)
    {
        std::copy_n(classic_table(), table_size, my_table);
        my_table[':'] = (mask)space;
    }
};
